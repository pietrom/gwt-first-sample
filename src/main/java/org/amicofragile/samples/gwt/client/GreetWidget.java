package org.amicofragile.samples.gwt.client;


import org.amicofragile.samples.gwt.shared.FieldVerifier;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class GreetWidget extends Composite {

	private static GreetWidgetUiBinder uiBinder = GWT
			.create(GreetWidgetUiBinder.class);

	interface GreetWidgetUiBinder extends UiBinder<Widget, GreetWidget> {
	}
	
	private static final String SERVER_ERROR = "An error occurred while "
	      + "attempting to contact the server. Please check your network "
	      + "connection and try again.";
	
	
	@UiField
	public Button sendButton;
	@UiField
	public TextBox nameField;
	@UiField
	public Label errorLabel;
	
	 private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	  private final Messages messages = GWT.create(Messages.class);
	
	
	public GreetWidget() {
		initWidget(uiBinder.createAndBindUi(this));
		nameField.setText( messages.nameField() );	
	    
	    
	    // Create the popup dialog box
	    final DialogBox dialogBox = new DialogBox();
	    dialogBox.setText("Remote Procedure Call");
	    dialogBox.setAnimationEnabled(true);
	    final Button closeButton = new Button("Close");
	    // We can set the id of a widget by accessing its Element
	    closeButton.getElement().setId("closeButton");
	    final Label textToServerLabel = new Label();
	    final HTML serverResponseLabel = new HTML();
	    VerticalPanel dialogVPanel = new VerticalPanel();
	    dialogVPanel.addStyleName("dialogVPanel");
	    dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
	    dialogVPanel.add(textToServerLabel);
	    dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
	    dialogVPanel.add(serverResponseLabel);
	    dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
	    dialogVPanel.add(closeButton);
	    dialogBox.setWidget(dialogVPanel);
	    
	  // Add a handler to close the DialogBox
	    closeButton.addClickHandler(new ClickHandler() {
	      public void onClick(ClickEvent event) {
	        dialogBox.hide();
	        sendButton.setEnabled(true);
	        sendButton.setFocus(true);
	      }
	    });
	    
	 // Create a handler for the sendButton and nameField
	    class GreetingUiHandler implements ClickHandler, KeyUpHandler {
	      /**
	       * Fired when the user clicks on the sendButton.
	       */
	      public void onClick(ClickEvent event) {
	        sendNameToServer();
	      }

	      /**
	       * Fired when the user types in the nameField.
	       */
	      public void onKeyUp(KeyUpEvent event) {
	        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
	          sendNameToServer();
	        }
	      }

	      /**
	       * Send the name from the nameField to the server and wait for a response.
	       */
	      private void sendNameToServer() {
	        // First, we validate the input.
	        errorLabel.setText("");
	        String textToServer = nameField.getText();
	        if (!FieldVerifier.isValidName(textToServer)) {
	          errorLabel.setText("Please enter at least four characters");
	          
	          return;
	        }

	        // Then, we send the input to the server.
	        sendButton.setEnabled(false);
	        textToServerLabel.setText(textToServer);
	        serverResponseLabel.setText("");
	        greetingService.greetServer(textToServer, new AsyncCallback<String>() {
	          public void onFailure(Throwable caught) {
	            // Show the RPC error message to the user
	            dialogBox.setText("Remote Procedure Call - Failure");
	            serverResponseLabel.addStyleName("serverResponseLabelError");
	            serverResponseLabel.setHTML(SERVER_ERROR);
	            dialogBox.center();
	            closeButton.setFocus(true);
	          }

	          public void onSuccess(String result) {
	            dialogBox.setText("Remote Procedure Call");
	            serverResponseLabel.removeStyleName("serverResponseLabelError");
	            serverResponseLabel.setHTML(result);
	            dialogBox.center();
	            closeButton.setFocus(true);
	          }
	        });
	      }
	    }
	    
	 // Add a handler to send the name to the server
	    GreetingUiHandler handler = new GreetingUiHandler();
	    sendButton.addClickHandler(handler);
	    nameField.addKeyUpHandler(handler);
         
	}
	
	

}
